public class Projectile {
    //declaring projectile classes variable
    private double mass;
    private double[] position;
    private double[] velocity;
    private double radius;
    private double angleShot;
    private double initialVelocityMagnitude;

// constructor
    public Projectile(double mass, double[] position, double radius,double angle,double initialVelocityMagnitude){
        this.mass = validateMass(mass);
        this.position = position.clone();
        this.radius = validateRadius(radius);
        this.angleShot = validateAngle(angle);
        this.initialVelocityMagnitude = validateVelocityMag(initialVelocityMagnitude);
        this.velocity = calculateInitialVelocity(); 
    }
    //helper function to validate angle
    public double validateAngle(double angle) {
    	  if (angle < 0 || angle > 90) {
              throw new IllegalArgumentException("Angle must be between 0 and 50.");
          }
          return angle;
	}
//helper function to validate mass
	public double validateMass(double mass) {
        if (mass < 0.1 || mass > 50) {
            throw new IllegalArgumentException("Mass must be between 0 and 50.");
        }
        return mass;
    }
    //helper function to validate initial velocity magnitude
	 public double validateVelocityMag(double Velocity) {
   	  if (Velocity < 0 || Velocity > 100) {
             throw new IllegalArgumentException("Velocity must be between 0 and 50.");
         }
         return Velocity;
	}
    //helper function to calculate initial velocity
    public double[] calculateInitialVelocity() {
        double projectileAngleRadians = Math.toRadians(getAngleShot());
        double initialVelocityX = getInitialVelocityMagnitude() * Math.cos(projectileAngleRadians);
        double initialVelocityY = getInitialVelocityMagnitude() * Math.sin(projectileAngleRadians);
        double initialVelocityZ = 0;
        return new double[]{initialVelocityX, initialVelocityY, initialVelocityZ};
    }
//helper function to validate radius
    public double validateRadius(double radius) {
        if (radius < 0.1 || radius > 20) {
            throw new IllegalArgumentException("Projectile radius must be between 0 and 20.");
        }
        return radius;
    }
    //getters
    public double getInitialVelocityMagnitude() {
        return initialVelocityMagnitude;
    }
    public double getAngleShot() {
        return angleShot;
    }
    public double getMass() {
        return mass;
    }
    public double getRadius(){
        return radius;
    }
//return projectile area calculated as per A = pi*r^2
    public double calculateProjectileArea() {
        return Math.PI * Math.pow(getRadius(), 2);
    }
    //getters
    public double[] getPosition() {
        return position;
    }

    public double[] getVelocity() {
        return velocity;
    }
// calculates drag force according to the formula given in the project doc 
    public double[] calculateDragForce(double C, double fluidDensity, double A, double[] flowVelocity) {
    	if (velocity.length != 3 || flowVelocity.length != 3) {
              throw new IllegalArgumentException("Velocity and flowVelocity arrays must have 3 elements.");
          }
   
        double[] dragForce = new double[3];
        for (int i = 0; i < 3; i++) {
            dragForce[i] = -0.5 * C * fluidDensity * A * Math.pow(velocity[i] - flowVelocity[i], 2);
        }
        return dragForce;
    }
}
