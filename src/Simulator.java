public class Simulator {
    /*
    Initializing variables for Simulator
     */
    private double timeStep;
    private double gravity;
    private double dragcoefficient;

// Constructor
    public Simulator(double timeStep, double gravityAccn, double dragCoeff) {
        this.timeStep = timeStep;
        this.gravity = validateGravityAccn(gravityAccn);
        this.dragcoefficient = validatedragCoeff(dragCoeff);
    }
// validate gravity acceleration
    public double validateGravityAccn (double gravityAccn) {
        if (gravityAccn != 9.81) {
            throw new IllegalArgumentException("Gravity has to be 9.81m/s^(2)");
        }
        return gravityAccn;
    }
// validate drag Coefficient  
    public double validatedragCoeff (double dragCoeff) {
        if (dragCoeff < 0.2 || dragCoeff > 0.5) {
            throw new IllegalArgumentException("DragCoefficient has to be between 0.2 to 0.5.");
        }
        return dragCoeff;
    }
// getters
    public double getdragcoefficient(){
    	return dragcoefficient;
    }
    
    public double getGravity(){
    	return gravity;
    }

// main projectile simulation function 
    public void simulateProjectile(Projectile projectile, double fluidDensity,
                                   double projectileArea, double[] flowVelocity,
                                   double[] externalForce, double totalTime) {
        double t = 0;
        while (t <= totalTime) {
            double[] dragForce = projectile.calculateDragForce(getdragcoefficient(), fluidDensity, projectileArea, flowVelocity);

            double[] totalForce = new double[3];
            for (int i = 0; i < 3; i++) {
              //  F = 𝑓(𝑡) + 𝑓d(𝑡) + 𝑚𝑔
                totalForce[i] = dragForce[i] + projectile.getMass() * getGravity() + externalForce[i];
            }

            double[] acceleration = new double[3];
            for (int i = 0; i < 3; i++) {
                acceleration[i] = totalForce[i] / projectile.getMass();
                projectile.getVelocity()[i] += acceleration[i] * timeStep;
                projectile.getPosition()[i] += projectile.getVelocity()[i] * timeStep;
            }

            t += timeStep;
        }
    }
// simulation and printing 
    public void simulateAndPrint(Projectile projectile, double fluidDensity,
                                 double projectileArea, double[] flowVelocity,
                                 double[] externalForce, double totalTime, double timeStep) {
        double currentTime = 0;
        while (currentTime <= totalTime) {
            simulateProjectile(projectile, fluidDensity, projectileArea, flowVelocity,
                               externalForce, currentTime);

            double[] currentPosition = projectile.getPosition();
            System.out.println("Time: " + currentTime + " s, Position (x, y, z): " + currentPosition[0] +
                    " m, " + currentPosition[1] + " m, " + currentPosition[2] + " m");
// checks if projectile has landed or not
            if (currentPosition[1] <= 0) {
                System.out.println("Projectile has landed.");
                break;
            }
            currentTime += timeStep;
        }
        double[] finalPosition = projectile.getPosition();
        System.out.println("Final position (x, y, z): " + finalPosition[0] + " m, " +
                finalPosition[1] + " m, " + finalPosition[2] + " m");
    }
}
