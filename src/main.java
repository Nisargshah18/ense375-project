public class main {
    public static void main(String[] args) {
        //intial values that can be modified by user to study different effects
        double mass = 43.0;
        double[] initialPosition = {10, 20, 0};
        double projectileAngleDegrees = 30.0;
        double initialVelocityMagnitude = 20;
        double[] flowVelocity = {10, 5, 0};
        double dragCoefficient = 0.5;
        double fluidDensity = 1.2;
        double projectileRadius = 1.5;
        double gravity = 9.81;
        double totalTime = 10;
        double timeStep = 0.01;
        double[] externalForce = {0, 0, 0};

// creating a projectile isntance followed by simulator instance using custom values and printing them out until projectile gets landed.
        Projectile projectile = new Projectile(mass, initialPosition, projectileRadius,projectileAngleDegrees,initialVelocityMagnitude);
        Simulator simulator = new Simulator(timeStep,gravity, dragCoefficient);
        simulator.simulateAndPrint(projectile, fluidDensity, projectile.calculateProjectileArea(), flowVelocity,externalForce, totalTime, timeStep);
    }
}
