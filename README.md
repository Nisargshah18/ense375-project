# ENSE375-Project

## Howitzer Firing Simulator

## <ins> Group Members <ins>

    - Nisarg Shah
    - Nikita Khavronin
    - Mubarak Abiola Keshiro

## <ins> Problem Specifications </ins>

    Create a simulator that accurately replicates the behavior of a howitzer firing a projectile across an empty field. The simulator should provide soldiers with a virtual environment where they can understand and manipulate various variables that affect the trajectory of the projectile, such as barrel pose, projectile mass, drag coefficient, initial speed, applied force, and gravity. This simulator should enable soldiers to observe and analyze the effects of these variables on the projectile's motion, allowing them to develop a better understanding of the principles of rigid body kinematics and dynamics. By using this simulator, soldiers will gain practical knowledge and skills necessary for accurately aiming and firing a howitzer in different scenarios, thereby enhancing their overall operational effectiveness and safety.

## <ins> Design Requirements </ins>

### Objectives

The objectives of the project to develop a Howitzer simulator for teaching soldiers about kinematics and dynamics are as follows: - Realistic Simulation: Develop an accurate simulator for howitzer operations, incorporating variables like barrel pose, projectile properties, and environmental factors.

    - Teach Kinematics and Dynamics: Enable soldiers to understand the principles of projectile motion, forces, and variables affecting trajectory through interactive simulations.

    - Safety Awareness: Educate soldiers on safety protocols and potential risks associated with howitzer operations within the simulator.

    - Skill Development: Provide opportunities for soldiers to practice aiming, adjusting variables, and enhance their proficiency in howitzer operations.

    - Cost-Effective Training: Reduce expenses by utilizing the simulator instead of live ammunition, optimizing resource allocation.

    - For Evaluation Purposes: Implement performance metrics to assess accuracy, decision-making, and provide feedback for improvement.

### Functions

For this simulator, there are a couple functionalities it needs to have. - The simulator needs to be able to calculate dyanmics and kinematics.It should be able to get the position, acceleration and velocity at every instance while taking drag force and gravity into account.

    - Manipulating and setting the barrel position on the x,y,z coordinates should be possible. The user should be able to do this.

    - It should be possible that the external forces and initial speed of the projectile can be set.

    - The projectile parameters such as the weight, dimensions and others. These should be able to affect the projectile's motion and behaviour.

    - Simulating Projectile Motion: This should be the main function that performs the simulation of the projectile's motion. It should calculate all contraints and parameters and be able to simulate its motion over time.

    - Display Projectile motion: The simulator should be able to show the user the result of the simulation including it's pattern, trajectory and entire motion process.

### Constraints

    - Numerical Accuracy: The howitzer simulator should prioritize accuracy in numerical calculations to provide soldiers with precise and reliable results. The calculations must accurately represent the kinematics and dynamics of the projectile's motion, ensuring that the simulated outputs align closely with real-world scenarios.

    - Economic Factors: The simulator should be developed within a reasonable budget, taking into account the costs associated with software development, hardware requirements, and maintenance. The affordability of the simulator needs to be taken into consideration for the Howitzer company, ensuring that the resources required for simulation align with their financial capabilities.

    - Effective Feedback: Design the simulator to provide clear, real-time feedback on the projectile's trajectory and the impact of different variables. The feedback should be user-friendly and informative, enabling soldiers to make informed decisions and adjustments during training exercises

    - Reliability: Design the simulator to be highly reliable and available, minimizing downtime and technical issues during training sessions. Conduct thorough testing and quality assurance measures to identify and resolve any potential bugs, errors, or performance issues.

### Class Diagram

![Alt text](/Documentation/Class_Diagram.jpeg)

### Testing Methods

    - Boundary value testing
    - Equivalence class testing
    - Decision table-based testing
    - Path testing
    - Dataflow

### Project Management

- Project Management Tool: Gantt Chart
- Version Control: GitLab
- IDE: Visual Studio Code, Eclipse, intelliJ
- Communication: Discord
- Documentation: GitLab ReadME.md
- Testing: Gitlab Testing.md

### Project Timeline

![Alt text](/Documentation/Gannt_Chart.png)

### Installation Instructions

- Clone the project from GitLab.
- Open Eclipse and select "File" > "Import".
- Select "Existing Projects into Workspace" and click "Next".
- Browse to the location where you cloned the project and select it.
- Click "Finish" to import the project into Eclipse.
- Run the project by selecting "Run" > "Run As" > "Java Application" from the menu.

### Usage Instructions

- Customize the <b>'mass', 'initialPosition', 'projectileAngleDegrees', 'initialVelocityMagnitude', 'flowVelocity', 'dragCoefficient', 'fluidDensity', 'projectileRadius', 'gravity', 'totalTime', 'timeStep', and 'externalForce'</b> variables according to your desired setup.
- <b>mass</b>: Set the projectile's mass in kilograms.
- <b>initialPosition</b>: Define the initial coordinates (x, y, z) of the projectile in meters.
- <b>projectileAngleDegrees</b>: Adjust the launch angle in degrees.
- <b>initialVelocityMagnitude</b>: Set the initial speed of the projectile in meters per second.
- <b>flowVelocity</b>: Specify the fluid flow velocity (x, y, z) in meters per second.
- <b>dragCoefficient</b>: Set the drag coefficient of the projectile.
- <b>fluidDensity</b>: Specify the fluid's density in kilograms per cubic meter.
- <b>projectileRadius</b>: Set the projectile's radius in meters.
- <b>gravity</b>: Adjust the acceleration due to gravity in meters per second squared.
- <b>totalTime</b>: Set the simulation duration in seconds.
  'timeStep': Adjust the time interval for simulation in seconds.
- <b>externalForce</b>: Set any additional external forces (x, y, z) on the projectile.
- Run the 'main' method to execute the simulation. The output will show the projectile's trajectory information over the 'totalTime' with the given 'timeStep'.

### Due Date

     August 02, 2023 23:59:59
