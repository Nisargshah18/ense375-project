# ENSE375-Project

## Howitzer Firing Simulator

## Testing Approach

### <ins> Boundary Value Testing </ins>

We tested the Projectile class that has four attributes: mass, radius, angle, and velocity. For each attribute, we have boundary value test cases to cover the minimum and maximum valid values, along with values just below the minimum and just above the maximum. We expect IllegalArgumentException to be thrown for the values outside the valid range.

### <ins> Equivalence Class Testing </ins>

The Simulator class is tested using equivalence class test cases for its attributes gravityAccn and dragCoeff. For gravityAccn, valid input with 9.81 is tested, and invalid inputs with 9.0 and 0.0 are tested, expecting IllegalArgumentException with corresponding error messages. For dragCoeff, valid input within the range 0.2 to 0.5 is tested, and invalid inputs below 0.2, above 0.5, and exactly 0 are tested, expecting IllegalArgumentException with appropriate messages. These test cases cover different equivalence classes and ensure proper handling of inputs in the Simulator class.

### <ins> Decision Table-Based Testing </ins>

Decision Table Testing is a black-box testing technique used to validate the behavior of a software system based on different combinations of input conditions and their corresponding expected outcomes. We are conducting decision table testing with three input conditions: mass, radius, and angle from Projectile's class.

### <ins> Path testing </ins>

Path testing is a white-box testing technique used on the Projectile class with the test class Pathtest. It aims to achieve 99% code coverage by testing different execution paths and logical conditions within the code. The testing includes validating mass, radius, angle, velocity, position calculations, and drag force to ensure correctness and reliability of the class implementation.

### <ins> Data Flow </ins>

Data flow testing with node coverage and DU pairs was performed on the Simulator class. The objective was to ensure comprehensive coverage of data flow paths and uncover potential data-related defects. By achieving high node coverage and testing DU pairs (Definition-Use pairs), various data flow scenarios were tested, ensuring data propagation correctness and identifying potential data-related issues for a robust Simulator class implementation.

### <ins> Integration Testing </ins>
In integration testing, we examine how different components collaborate and interact within the system. Here, the Projectile and Simulator classes are integrated to ensure their smooth functioning as a cohesive unit when simulating the projectile's behavior under various environmental conditions. The focus is on verifying the seamless interaction between these classes, ultimately ensuring that the system works correctly as a whole.

### <ins> System Testing </ins>
System testing ensures that the Projectile class and the Simulator class work together harmoniously, and all the components of the system function correctly in real-world scenarios. The integration between these classes is tested, and the correctness of their interactions is validated, providing confidence in the overall system's performance and accuracy.
