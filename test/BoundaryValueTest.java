import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class BoundaryValueTest {
    @Test
    /*
    Boundary value Test conducted on different values of projectile such as mass, Radius, angle and initial Velocity magnitude.
    Testing values: 
                1) 0.1  <= Mass <= 50
                2) 0.1 <= radius <= 20
                3) 0 <= angle <= 90
                4) 0 <= initialVelocityMagnitude <= 100 
    The test names are quite explanatory. For failures, I am throwihg exceptions to let user know what caused the failure. The try catches are designed in a
    way to check if they work they are supposed to or not. 
      
     */
    public void testMassBoundaryValues() {
        //initializes a projectile instance
        Projectile projectile1 = new Projectile(0.1, new double[]{0, 0, 0}, 1, 30, 10);
        //actual = expected???
        assertEquals(0.1, projectile1.getMass(), 0.001);
        
        Projectile projectile2 = new Projectile(50, new double[]{0, 0, 0}, 1, 30, 10);
        assertEquals(50, projectile2.getMass(), 0.001);

        try {
            Projectile projectile3 = new Projectile(0.099, new double[]{0, 0, 0}, 1, 30, 10);
            fail("Expected IllegalArgumentException");
        } catch (IllegalArgumentException e) {
           assertEquals(e.getMessage(),"Mass must be between 0 and 50.");
        }

        try {
            Projectile projectile4 = new Projectile(50.001, new double[]{0, 0, 0}, 1, 30, 10);
            fail("Expected IllegalArgumentException");
        } catch (IllegalArgumentException e) {
        	 assertEquals(e.getMessage(),"Mass must be between 0 and 50.");
        }
    }
    
     // Test the boundary values for radius
    @Test
    public void testRadiusBoundaryValues() {
        //initializes a projectile instance
        Projectile projectile1 = new Projectile(10, new double[]{0, 0, 0}, 1, 5, 5);
        //actual = expected???
        assertEquals(1, projectile1.getRadius(), 0.0001);

        Projectile projectile2 = new Projectile(10, new double[]{0, 0, 0}, 19, 30, 10);
        assertEquals(19, projectile2.getRadius(), 0.0001);

        try {
            Projectile projectile3 = new Projectile(10, new double[]{0, 0, 0}, -0.001, 30, 10);
            fail("Expected IllegalArgumentException");
        } catch (IllegalArgumentException e) {
        	assertEquals(e.getMessage(),"Projectile radius must be between 0 and 20.");
        }

        try {
            Projectile projectile4 = new Projectile(10, new double[]{0, 0, 0}, 20.001, 30, 10);
            fail("Expected IllegalArgumentException");
        } catch (IllegalArgumentException e) {
        	assertEquals(e.getMessage(),"Projectile radius must be between 0 and 20.");
        }
    }
    
    // Test the boundary values for angle
    @Test
    public void testAngleBoundaryValues() {
        //initializes a projectile instance
        Projectile projectile1 = new Projectile(10, new double[]{0, 0, 0}, 1, 0, 10);
        //actual = expected???
        assertEquals(0, projectile1.getAngleShot(), 0.0001);

        Projectile projectile2 = new Projectile(10, new double[]{0, 0, 0}, 1, 90, 10);
        assertEquals(90, projectile2.getAngleShot(), 0.0001);

        try {
            Projectile projectile3 = new Projectile(10, new double[]{0, 0, 0}, 1, -1, 10);
            fail("Expected IllegalArgumentException");
        } catch (IllegalArgumentException e) {
           
        	assertEquals(e.getMessage(),"Angle must be between 0 and 50.");
        }

        try {
            Projectile projectile4 = new Projectile(10, new double[]{0, 0, 0}, 1, 91, 10);
            fail("Expected IllegalArgumentException");
        } catch (IllegalArgumentException e) {
        	assertEquals(e.getMessage(),"Angle must be between 0 and 50.");
        }
    }
 
 //Test the boundary values of initial velocity magnitude
    @Test
    public void testInitialVelocityMagnitudeBoundaryValues() {
        //initializes a projectile instance
        Projectile projectile1 = new Projectile(10, new double[]{0, 0, 0}, 1, 30, 0);
        //actual = expected???
        assertEquals(0, projectile1.getInitialVelocityMagnitude(), 0.0001);

        Projectile projectile2 = new Projectile(10, new double[]{0, 0, 0}, 1, 30, 100);
        assertEquals(100, projectile2.getInitialVelocityMagnitude(), 0.0001);

        try {
            Projectile projectile3 = new Projectile(10, new double[]{0, 0, 0}, 1, 30, -0.001);
            fail("Expected IllegalArgumentException");
        } catch (IllegalArgumentException e) {
        	assertEquals(e.getMessage(),"Velocity must be between 0 and 50.");
        }

        try {
            Projectile projectile4 = new Projectile(10, new double[]{0, 0, 0}, 1, 30, 100.001);
            fail("Expected IllegalArgumentException");
        } catch (IllegalArgumentException e) {
        	assertEquals(e.getMessage(),"Velocity must be between 0 and 50.");
        }
    }

}




  
