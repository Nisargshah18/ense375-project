import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SystemTest {

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	double mass = 43.0;
    double[] initialPosition = {10, 20, 0};
    double projectileAngleDegrees = 30.0;
    double initialVelocityMagnitude = 20;
    double[] flowVelocity = {10, 5, 0};
    double dragCoefficient = 0.5;
    double fluidDensity = 1.2;

    double projectileRadius = 1.5;

    double gravity = 9.81;
    double totalTime = 10;
    double timeStep = 0.01;

    double[] externalForce = {0, 0, 0};
    @Test
    /*
     System testing is a level of software testing where the entire software system is tested as a whole to verify that all components/modules work together correctly and meet the specified requirements. 
     The purpose of system testing is to validate the complete system's behavior and ensure that it fulfills the intended functionality.
     */
    public void testBasicProjectileMotion() {
        Projectile projectile = new Projectile(mass, initialPosition, projectileRadius, projectileAngleDegrees, initialVelocityMagnitude);
        Simulator simulator = new Simulator(timeStep, gravity, dragCoefficient);
        simulator.simulateProjectile(projectile, fluidDensity, projectile.calculateProjectileArea(), flowVelocity, externalForce, totalTime);

        double[] finalPosition = projectile.getPosition();
        assertEquals(245.766, finalPosition[0], 0.001);
        assertEquals(203.410, finalPosition[1], 0.001);
        assertEquals(127.239, finalPosition[2], 0.001);
    }

    @Test
    public void testInvalidInputValidation() {
        try {
            Projectile invalidMassProjectile = new Projectile(0.05, initialPosition, projectileRadius, projectileAngleDegrees, initialVelocityMagnitude);
            fail("Expected an IllegalArgumentException to be thrown for invalid mass");
        } catch (IllegalArgumentException e) {
            assertTrue(true);
        }

        try {
            Projectile invalidAngleProjectile = new Projectile(mass, initialPosition, projectileRadius, 100, initialVelocityMagnitude);
            fail("Expected an IllegalArgumentException to be thrown for invalid angle");
        } catch (IllegalArgumentException e) {
            assertTrue(true);
        }

        try {

            Projectile invalidVelocityProjectile = new Projectile(mass, initialPosition, projectileRadius, projectileAngleDegrees, 110);
            fail("Expected an IllegalArgumentException to be thrown for invalid initial velocity");
        } catch (IllegalArgumentException e) {
            assertTrue(true);
        }
    }

    @Test
    public void testCalculateInitialVelocity() {
        double mass = 43.0;
        double projectileAngleDegrees = 30.0;
        double initialVelocityMagnitude = 20;

        Projectile projectile = new Projectile(mass, initialPosition, projectileRadius, projectileAngleDegrees, initialVelocityMagnitude);

        double[] initialVelocity = projectile.calculateInitialVelocity();
        assertEquals(17.3205, initialVelocity[0], 0.001);
        assertEquals(10.0, initialVelocity[1], 0.001);
        assertEquals(0, initialVelocity[2], 0.001);
    }

    @Test
    public void testCalculateDragForce() {
        double mass = 43.0;
        double projectileAngleDegrees = 30.0;
        double initialVelocityMagnitude = 20;
        double fluidDensity = 1.2;
        double[] flowVelocity = {10, 5, 0};
        double dragCoefficient = 0.5;
        double projectileArea = Math.PI * Math.pow(projectileRadius, 2);

        Projectile projectile = new Projectile(mass, initialPosition, projectileRadius, projectileAngleDegrees, initialVelocityMagnitude);
        Simulator simulator = new Simulator(timeStep, gravity, dragCoefficient);

        double[] dragForce = projectile.calculateDragForce(simulator.getdragcoefficient(), fluidDensity, projectileArea, flowVelocity);

        assertEquals(-113.641, dragForce[0], 0.001);
        assertEquals(-53.014, dragForce[1], 0.001);
        assertEquals(0, dragForce[2], 0.001);
    }

}
