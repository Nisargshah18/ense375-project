import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


class EquivalenceClassTest {
    @Test
    /*
    We carried out Equivalence Test on major variables of Simulator Class Gravity Acceleration and DragCoefficient. 
                    1) Gravity Acceleration = 9.81 m/s^2
                    2) 0.2 <= dragCoeff <= 0.5 

    Once again, we have utilized try catches in order to let the users know what caused the error with a proper message. 
     */
     // Test class for Gravity Acceleration
    public void testValidateGravityAccn() {
        try {
            // Equivalence Class 1: gravityAccn == 9.81 (Valid input)
            //Initializing a simulator class instance
            Simulator validGravitySimulator = new Simulator(0.01, 9.81, 0.3);
            // actual == expected ???
            assertEquals(9.81, validGravitySimulator.getGravity());
        } catch (IllegalArgumentException e) {
            fail("Unexpected IllegalArgumentException for gravityAccn == 9.81.");
        }

        try {
            // Equivalence Class 2: gravityAccn != 9.81 (Invalid input)
            Simulator invalidGravitySimulator = new Simulator(0.01, 9.0, 0.3);
            fail("Expected IllegalArgumentException for gravityAccn != 9.81, but no exception was thrown.");
        } catch (IllegalArgumentException e) {
        	assertEquals(e.getMessage(),"Gravity has to be 9.81m/s^(2)");
        }
        
        try {
            // Equivalence Class 3: gravityAccn != 0 (Invalid input)
            Simulator invalidGravitySimulator = new Simulator(0.01, 0.0, 0.3);
            fail("Expected IllegalArgumentException for gravityAccn != 9.81, but no exception was thrown.");
        } catch (IllegalArgumentException e) {
        	assertEquals(e.getMessage(),"Gravity has to be 9.81m/s^(2)");
        }        
    }
    
    @Test
    // Test class for Drag Coefficient
    public void testValidateDragCoeff() {
        try {
            // Equivalence Class 1: 0.2 <= dragCoeff <= 0.5 (Valid input)
            //Initializing a simulator class instance
            Simulator validDragSimulator = new Simulator(0.01, 9.81, 0.3);
            // actual == expected ???
            assertEquals(0.3, validDragSimulator.getdragcoefficient());
        } catch (IllegalArgumentException e) {
            fail("Unexpected IllegalArgumentException for 0.2 <= dragCoeff <= 0.5.");
        }
       
        try {
            // Equivalence Class 2: dragCoeff < 0.2 (Invalid input)
            Simulator invalidLowDragSimulator = new Simulator(0.01, 9.81, 0.1);
            fail("Expected IllegalArgumentException for dragCoeff < 0.2, but no exception was thrown.");
        } catch (IllegalArgumentException e) {
        	assertEquals(e.getMessage(),"DragCoefficient has to be between 0.2 to 0.5.");
        }

        try {
            // Equivalence Class 3: dragCoeff > 0.5 (Invalid input)
            Simulator invalidHighDragSimulator = new Simulator(0.01, 9.81, 0.6);
            fail("Expected IllegalArgumentException for dragCoeff > 0.5, but no exception was thrown.");
        } catch (IllegalArgumentException e) {
        	assertEquals(e.getMessage(),"DragCoefficient has to be between 0.2 to 0.5.");
        }
        
        try {
            // Equivalence Class 4: dragCoeff = 0 Invalid input)
            Simulator validDragSimulator = new Simulator(0.01, 9.81, 0);
            assertEquals(0.3, validDragSimulator.getdragcoefficient());
        } catch (IllegalArgumentException e) {
        	assertEquals(e.getMessage(),"DragCoefficient has to be between 0.2 to 0.5.");
        }

    }
}