import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class IntegrationTest {

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}
	  @Test
	    public void testSimulateProjectileIntegration() {
			/*
			Here we are testing simulator class with two different classes between Simulator class and Projectile Class.
			 */
	        double mass = 43.0;
	        double[] initialPosition = {10, 20, 0};
	        double projectileAngleDegrees = 30.0;
	        double initialVelocityMagnitude = 20;
	        double[] flowVelocity = {10, 5, 0};
	        double dragCoefficient = 0.5;
	        double fluidDensity = 1.2;

	        double projectileRadius = 1.5;

	        double gravity = 9.81;
	        double totalTime = 10;
	        double timeStep = 0.01;

	        double[] externalForce = {0, 0, 0};

	        Projectile projectile = new Projectile(mass, initialPosition, projectileRadius, projectileAngleDegrees, initialVelocityMagnitude);
	        Simulator simulator = new Simulator(timeStep, gravity, dragCoefficient);

	        simulator.simulateProjectile(projectile, fluidDensity, projectile.calculateProjectileArea(), flowVelocity, externalForce, totalTime);

	        double[] finalPosition = projectile.getPosition();
	        assertEquals(245.766, finalPosition[0], 0.001);
	        assertEquals(203.410, finalPosition[1], 0.001);
	        assertEquals(127.239, finalPosition[2], 0.001);

	        assertTrue(finalPosition[1] >= 0);
	    }

	    @Test
	    public void testSimulateProjectileWithExternalForce() {
	        double mass = 20.0;
	        double[] initialPosition = {0, 0, 0};
	        double projectileAngleDegrees = 45.0;
	        double initialVelocityMagnitude = 10;
	        double[] flowVelocity = {0, 0, 0};
	        double dragCoefficient = 0.2;
	        double fluidDensity = 1.0;

	        double projectileRadius = 2.0;

	        double gravity = 9.81;
	        double totalTime = 5;
	        double timeStep = 0.01;

	        double[] externalForce = {10, 5, 0};

	        Projectile projectile = new Projectile(mass, initialPosition, projectileRadius, projectileAngleDegrees, initialVelocityMagnitude);
	        Simulator simulator = new Simulator(timeStep, gravity, dragCoefficient);

	        simulator.simulateProjectile(projectile, fluidDensity, projectile.calculateProjectileArea(), flowVelocity, externalForce, totalTime);

	        double[] finalPosition = projectile.getPosition();
	        assertEquals(60.195, finalPosition[0], 0.001);
	        assertEquals(59.481, finalPosition[1], 0.001);
	        assertEquals(51.675, finalPosition[2], 0.001);

	        assertTrue(finalPosition[1] >= 0);
	    }

	    @Test
	    public void testSimulateProjectileWithHighDragCoefficient() {
	        double mass = 30.0;
	        double[] initialPosition = {5, 15, 0};
	        double projectileAngleDegrees = 60.0;
	        double initialVelocityMagnitude = 15;
	        double[] flowVelocity = {0, 0, 0};
	        double dragCoefficient = 0.5;
	        double fluidDensity = 1.2;

	        double projectileRadius = 1.0;

	        double gravity = 9.81;
	        double totalTime = 10;
	        double timeStep = 0.01;

	        double[] externalForce = {0, 0, 0};

	        Projectile projectile = new Projectile(mass, initialPosition, projectileRadius, projectileAngleDegrees, initialVelocityMagnitude);
	        Simulator simulator = new Simulator(timeStep, gravity, dragCoefficient);

	        simulator.simulateProjectile(projectile, fluidDensity, projectile.calculateProjectileArea(), flowVelocity, externalForce, totalTime);

	        double[] finalPosition = projectile.getPosition();
	        assertEquals(171.176, finalPosition[0], 0.001);
	        assertEquals(187.409, finalPosition[1], 0.001);
	        assertEquals(154.965, finalPosition[2], 0.001);

	        assertTrue(finalPosition[1] >= 0);
	    }
}
