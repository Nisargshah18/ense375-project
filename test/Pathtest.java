import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class Pathtest {
	//Use eclipse emma to test one class make sure to add more if to get more code coverage

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterEach
	void tearDown() throws Exception {
	}

	/*
	 * We use Eclipse Emmacode to do the path test of Projectile class. A lot of tests came from our previous tests such as boundary value test and equivalence class test to make sure we cover most of the paths.
	 The path coverage reported by eclipse emma showed 99% code coverage. We successfully cover all paths by meeting conditions from loops and conditional statements.
	 * */
	
		@Test
		  public void testMassValues() {
		       //initializing instance of projectile
		        Projectile projectile1 = new Projectile(0.1, new double[]{0, 0, 0}, 1, 30, 10);
				//actual == expected???? 
		        assertEquals(0.1, projectile1.getMass(), 0.001);

		        Projectile projectile2 = new Projectile(50, new double[]{0, 0, 0}, 1, 30, 10);
		        assertEquals(50, projectile2.getMass(), 0.001);

		        try {
		            Projectile projectile3 = new Projectile(0.099, new double[]{0, 0, 0}, 1, 30, 10);
		            fail("Expected IllegalArgumentException");
		        } catch (IllegalArgumentException e) {
		           assertEquals(e.getMessage(),"Mass must be between 0 and 50.");
		        }
		        try {
		            Projectile projectile4 = new Projectile(50.001, new double[]{0, 0, 0}, 1, 30, 10);
		            fail("Expected IllegalArgumentException");
		        } catch (IllegalArgumentException e) {
		        	 assertEquals(e.getMessage(),"Mass must be between 0 and 50.");
		        }
		    }
		    
		    
		    @Test
		    public void testRadiusValues() {
		        //initializing instance of projectile
		        Projectile projectile1 = new Projectile(10, new double[]{0, 0, 0}, 1, 5, 5);
				//actual == expected???? 
		        assertEquals(1, projectile1.getRadius(), 0.0001);

		        Projectile projectile2 = new Projectile(10, new double[]{0, 0, 0}, 19, 30, 10);
		        assertEquals(19, projectile2.getRadius(), 0.0001);

		        try {
		            Projectile projectile3 = new Projectile(10, new double[]{0, 0, 0}, -0.001, 30, 10);
		            fail("Expected IllegalArgumentException");
		        } catch (IllegalArgumentException e) {
		        	assertEquals(e.getMessage(),"Projectile radius must be between 0 and 20.");
		        }

		        try {
		            Projectile projectile4 = new Projectile(10, new double[]{0, 0, 0}, 20.001, 30, 10);
		            fail("Expected IllegalArgumentException");
		        } catch (IllegalArgumentException e) {
		        	assertEquals(e.getMessage(),"Projectile radius must be between 0 and 20.");
		        }
		    }
		    
		    @Test
		    public void testAngleValues() {
		          //initializing instance of projectile
		        Projectile projectile1 = new Projectile(10, new double[]{0, 0, 0}, 1, 0, 10);
				//actual == expected???? 
		        assertEquals(0, projectile1.getAngleShot(), 0.0001);

		        // Maximum valid value for angle
		        Projectile projectile2 = new Projectile(10, new double[]{0, 0, 0}, 1, 90, 10);
		        assertEquals(90, projectile2.getAngleShot(), 0.0001);

		        try {
		            Projectile projectile3 = new Projectile(10, new double[]{0, 0, 0}, 1, -1, 10);
		            fail("Expected IllegalArgumentException");
		        } catch (IllegalArgumentException e) {
		           
		        	assertEquals(e.getMessage(),"Angle must be between 0 and 50.");
		        }

		        try {
		            Projectile projectile4 = new Projectile(10, new double[]{0, 0, 0}, 1, 91, 10);
		            fail("Expected IllegalArgumentException");
		        } catch (IllegalArgumentException e) {
		        	assertEquals(e.getMessage(),"Angle must be between 0 and 50.");
		        }
		    }
		    
		    
		    
		    
		    @Test
		    public void testCalculateProjectileArea() {
		        double radius = 2.5;
				  //initializing instance of projectile
		        Projectile projectile = new Projectile(1.0, new double[]{0, 0, 0}, radius, 45.0, 10.0);
		        
		        double expectedArea =  Math.PI * Math.pow(radius, 2);
		        
		        double calculatedArea = projectile.calculateProjectileArea();
		        
		          //actual == expected??? but with tolerance of 0.0001
		        assertEquals(expectedArea, calculatedArea, 0.0001);
		    }
		    
		    
		    
		    @Test
		    public void testInitialVelocityMagnitudeValues() {
		      //initializing instance of projectile
		        Projectile projectile1 = new Projectile(10, new double[]{0, 0, 0}, 1, 30, 0);
				 //actual == expected???
		        assertEquals(0, projectile1.getInitialVelocityMagnitude(), 0.0001);

		      
		        Projectile projectile2 = new Projectile(10, new double[]{0, 0, 0}, 1, 30, 100);
		        assertEquals(100, projectile2.getInitialVelocityMagnitude(), 0.0001);

		        try {
		            Projectile projectile3 = new Projectile(10, new double[]{0, 0, 0}, 1, 30, -0.001);
		            fail("Expected IllegalArgumentException");
		        } catch (IllegalArgumentException e) {
		        	assertEquals(e.getMessage(),"Velocity must be between 0 and 50.");
		        }

		        try {
		            Projectile projectile4 = new Projectile(10, new double[]{0, 0, 0}, 1, 30, 100.001);
		            fail("Expected IllegalArgumentException");
		        } catch (IllegalArgumentException e) {
		        	assertEquals(e.getMessage(),"Velocity must be between 0 and 50.");
		        }
		    }
		    
		    @Test
		    public void testGetVelocity() {
		        double[] initialPosition = {1.0, 2.0, 3.0};
		        Projectile projectile = new Projectile(1.0, initialPosition, 1.0, 45.0, 10.0);

		        double[] velocity = projectile.getVelocity();
		        
		        double projectileAngleRadians = Math.toRadians(45.0);
		        double initialVelocityX = 10.0 * Math.cos(projectileAngleRadians);
		        double initialVelocityY = 10.0 * Math.sin(projectileAngleRadians);
		        double initialVelocityZ = 0;
		        double[] calculatedArray = new double[]{initialVelocityX, initialVelocityY, initialVelocityZ};
		        assertArrayEquals(velocity, calculatedArray);

		        
		    }
		    
		    @Test
		    public void testGetPosition() {
		        double[] initialPosition = {1.0, 2.0, 3.0};
		        Projectile projectile = new Projectile(1.0, initialPosition, 1.0, 45.0, 10.0);

		        double[] position = projectile.getPosition();

		        // Check that the returned position array is not the same object reference as the original one
		        assertNotSame(initialPosition, position);

		        // Check that the contents of the returned position array are the same as the original one
		        assertArrayEquals(initialPosition, position);
		    }
		    


		    @Test
		    public void testCalculateDragForce() {
		        /*Valid Test Case*/
		        double mass = 0.5; 
		        double[] position = {0, 0, 0};
		        double radius = 0.2;
		        double angle = 45;
		        double initialVelocityMagnitude = 30;
		        Projectile projectile = new Projectile(mass, position, radius, angle, initialVelocityMagnitude);
		        double C = 0.4; 
		        double fluidDensity = 1.2; 
		        double A = projectile.calculateProjectileArea();
		        double[] flowVelocity = {10, 0, 0}; 
		        double[] expectedDragForce = new double[3];
		        for (int i = 0; i < 3; i++) {
		            expectedDragForce[i] = -0.5 * C * fluidDensity * A * Math.pow(projectile.getVelocity()[i] - flowVelocity[i], 2);
		        }

		        double[] actualDragForce = projectile.calculateDragForce(C, fluidDensity, A, flowVelocity);
		        // Delta 0.0001 for a better approximation of the value
		        for (int i = 0; i < 3; i++) {
		            assertEquals(expectedDragForce[i], actualDragForce[i], 0.0001); 
		        }
		    }
		    
		    
		    
}
